<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://wordpress.org/support/article/editing-wp-config-php/

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', 'unicainstancia' );


/** MySQL database username */

define( 'DB_USER', 'root' );


/** MySQL database password */

define( 'DB_PASSWORD', '' );


/** MySQL hostname */

define( 'DB_HOST', '127.0.0.1' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         'Z|oxO)-P^tGo& 7+*+=|U~:=`#XlD|>z>k;{Z5dIBE@M7Idaio:/8xq[QAQb9P0z' );

define( 'SECURE_AUTH_KEY',  'A {+0t|#;F 9A^Y$J#((i&RDY%)MU$n0t{}|,3n)t~[`lS;!<c,&u-mB8!@p[wEN' );

define( 'LOGGED_IN_KEY',    '+Oj`J:h=4YeqG$F>N)oN4[@^T5DLNMo;s,FXIklzQ9yJ3L0X9~9:n(<DTc,wR4~[' );

define( 'NONCE_KEY',        '6MoX+61Pt@0QN5clRSU@fgqp_&MhI8{@JK  uyB^shK#.6EU-+bdj;7bvx1.5H4h' );

define( 'AUTH_SALT',        't%FgKJ,9.<F]]Ry$x2yVnK+jRmZZjZ@l}d3y+.K3rKR;L;57>VKf]iIsT!8M>>!G' );

define( 'SECURE_AUTH_SALT', 't[|E {f| G7E4CHU&e]:)fhPc{}EA/rw`BiTdUsOXo.;i?juo|l7e6d7|X68c4?h' );

define( 'LOGGED_IN_SALT',   'i>V8Jt&wFk@3,TK{K[RmVD,MZhF-<B>dM_he/k{+S[PC;_V8gma!-[:w~NTA*9ED' );

define( 'NONCE_SALT',       'D^+LWZ7y$hW%*KXrx)&HhJW?9pK1Y_,IeP}_bl6Oy%>TYKI.Gdq,aAwW`G*/@*Z0' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the documentation.

 *

 * @link https://wordpress.org/support/article/debugging-in-wordpress/

 */

define( 'WP_DEBUG', false );


define( 'FS_METHOD', 'direct' );
/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','http://example.com');
 *  define('WP_SITEURL','http://example.com');
 *
 */
if ( defined( 'WP_CLI' ) ) {
	$_SERVER['HTTP_HOST'] = '127.0.0.1';
}

define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/' );
define( 'WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/' );
define( 'WP_AUTO_UPDATE_CORE', 'minor' );
/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', __DIR__ . '/' );

}


/** Sets up WordPress vars and included files. */

require_once ABSPATH . 'wp-settings.php';

/**
 * Disable pingback.ping xmlrpc method to prevent WordPress from participating in DDoS attacks
 * More info at: https://docs.bitnami.com/general/apps/wordpress/troubleshooting/xmlrpc-and-pingback/
 */
if ( !defined( 'WP_CLI' ) ) {
	// remove x-pingback HTTP header
	add_filter("wp_headers", function($headers) {
		unset($headers["X-Pingback"]);
		return $headers;
	});
	// disable pingbacks
	add_filter( "xmlrpc_methods", function( $methods ) {
		unset( $methods["pingback.ping"] );
		return $methods;
	});
}
